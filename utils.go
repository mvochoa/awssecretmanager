package awssecretmanager

import (
	"fmt"
	"os"
)

func checkError(err error, tag string, v ...interface{}) {
	if err != nil {
		tag = fmt.Sprintf(tag, v...)
		fmt.Fprintf(os.Stderr, "Error/%s: %v\n", tag, err.Error())
		os.Exit(1)
	}
}
