package awssecretmanager

import (
	"encoding/base64"
	"log"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/secretsmanager"
)

func NewSessionSecretManager(region string) *secretsmanager.SecretsManager {
	session, err := session.NewSessionWithOptions(session.Options{
		Config: aws.Config{
			Region: aws.String("us-west-1"),
		},
		SharedConfigState: session.SharedConfigEnable,
	})
	if err != nil {
		log.Panic(err)
	}

	awsConfig := aws.NewConfig()
	if region != "" {
		awsConfig.WithRegion(region)
	}

	return secretsmanager.New(session, awsConfig)
}

func GetSecret(sessionSecretManager *secretsmanager.SecretsManager, secretId string) string {
	result, err := sessionSecretManager.GetSecretValue(&secretsmanager.GetSecretValueInput{
		SecretId:     aws.String(secretId),
		VersionStage: aws.String("AWSCURRENT"),
	})
	if err != nil {
		if awsError, ok := err.(awserr.Error); ok {
			switch awsError.Code() {
			case secretsmanager.ErrCodeDecryptionFailure:
				checkError(awsError, "DecryptionFailure (%s)", secretId)

			case secretsmanager.ErrCodeInternalServiceError:
				checkError(awsError, "InternalService (%s)", secretId)

			case secretsmanager.ErrCodeInvalidParameterException:
				checkError(awsError, "InvalidParameter (%s)", secretId)

			case secretsmanager.ErrCodeInvalidRequestException:
				checkError(awsError, "InvalidRequest (%s)", secretId)

			case secretsmanager.ErrCodeResourceNotFoundException:
				checkError(awsError, "ResourceNotFound (%s)", secretId)
			}
		}
		checkError(err, "Unknown (%s)", secretId)
	}

	var secretString string
	if result.SecretString != nil {
		secretString = *result.SecretString
	} else {
		decodedBinarySecretBytes := make([]byte, base64.StdEncoding.DecodedLen(len(result.SecretBinary)))
		len, err := base64.StdEncoding.Decode(decodedBinarySecretBytes, result.SecretBinary)
		if err != nil {
			checkError(err, "Unknown (%s)", secretId)
			log.Panic(err)
		}
		secretString = string(decodedBinarySecretBytes[:len])
	}

	return secretString
}
