package awssecretmanager

import (
	"reflect"
	"testing"
)

func TestSecretGenerator(t *testing.T) {
	var p Plugin
	config := []byte(`apiVersion: hi.example.com/v1
kind: SecretGenerationSecretManager
metadata:
  name: my-secret
spec:
  regionAWS: us-west-2`)
	p.Config(config)
	got := p.Generate()
	want := []byte(`apiVersion: v1
data: {}
kind: Secret
metadata:
  annotations:
    internal.config.kubernetes.io/generatorBehavior: unspecified
    internal.config.kubernetes.io/needsHashSuffix: enabled
  name: my-secret
type: Opaque
`)
	if !reflect.DeepEqual(got, want) {
		t.Errorf("Not equal \n-----\n%s\n====\n\n-----\n%s\n-----\n", got, want)
	}
}
