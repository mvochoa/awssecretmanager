package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"

	"gitlab.com/mvochoa/awssecretmanager"
)

var file string

func commands() {
	flag.Parse()
	file = flag.Arg(0)
	if file == "" {
		fmt.Fprintf(os.Stderr, "Usage of %s:\n\n", os.Args[0])
		fmt.Fprintf(os.Stderr, "Example: %s file.yaml\n\n", os.Args[0])
		fmt.Print("Arguments:\n\n")
		flag.PrintDefaults()
		os.Exit(1)
	}
}

func main() {
	commands()
	var plugin awssecretmanager.Plugin
	b, _ := ioutil.ReadFile(file)
	plugin.Config(b)
	fmt.Printf("%s", plugin.Generate())
}
