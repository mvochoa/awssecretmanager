package awssecretmanager

import (
	"encoding/json"
	"fmt"

	"sigs.k8s.io/kustomize/api/kv"
	"sigs.k8s.io/kustomize/api/provider"
	"sigs.k8s.io/kustomize/api/resmap"
	"sigs.k8s.io/kustomize/api/types"
	"sigs.k8s.io/yaml"
)

type Value struct {
	Key  string `json:"key" yaml:"key"`
	Name string `json:"name" yaml:"name"`
}

type Secret struct {
	Name        string  `json:"name" yaml:"name"`
	Rename      string  `json:"rename" yaml:"rename"`
	IsPlainText bool    `json:"isPlainText" yaml:"isPlainText"`
	Values      []Value `json:"values" yaml:"values"`
}

type Spec struct {
	RegionAws string   `json:"regionAws" yaml:"regionAws"`
	Secrets   []Secret `json:"secrets" yaml:"secrets"`
}

type Plugin struct {
	h                *resmap.PluginHelpers
	types.ObjectMeta `json:"metadata,omitempty" yaml:"metadata,omitempty"`
	Behavior         string `json:"behavior,omitempty" yaml:"behavior,omitempty"`
	Spec             `json:"spec" yaml:"spec"`
}

func (p *Plugin) Config(config []byte) (err error) {
	provider := provider.NewDefaultDepProvider()
	resmapFactory := resmap.NewFactory(provider.GetResourceFactory())
	p.h = resmap.NewPluginHelpers(nil, provider.GetFieldValidator(), resmapFactory, nil)

	if p.Annotations == nil {
		p.Annotations = make(map[string]string)
	}

	return yaml.Unmarshal(config, p)
}

func (p *Plugin) Generate() []byte {
	args := types.SecretArgs{}
	args.Name = p.Name
	args.Namespace = p.Namespace
	if p.Behavior != "" {
		args.Behavior = p.Behavior
		p.Annotations["kustomize.config.k8s.io/behavior"] = p.Behavior
	}

	args.Options = &types.GeneratorOptions{
		Annotations: p.Annotations,
	}

	sessionSecretManager := NewSessionSecretManager(p.RegionAws)
	for _, d := range p.Spec.Secrets {
		secret := GetSecret(sessionSecretManager, d.Name)
		if d.IsPlainText {
			if d.Rename == "" {
				d.Rename = d.Name
			}
			value := fmt.Sprintf("%v=%v", d.Rename, secret)
			args.LiteralSources = append(args.LiteralSources, value)
			continue
		}
		for _, v := range d.Values {
			var m map[string]interface{}
			err := json.Unmarshal([]byte(secret), &m)
			checkError(err, "Unmarshal JSON (%s)", d.Name)
			value := fmt.Sprintf("%v=%v", v.Name, m[v.Key])
			args.LiteralSources = append(args.LiteralSources, value)
		}
	}

	secret, _ := p.h.ResmapFactory().FromSecretArgs(
		kv.NewLoader(p.h.Loader(), p.h.Validator()), args)
	result, _ := secret.AsYaml()
	return result
}
