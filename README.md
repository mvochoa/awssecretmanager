# AWS Parameter Store

Kustomize plugin to AWS Secret Manager

Download binary [release](https://gitlab.com/mvochoa/awssecretmanager/-/releases) 

## Install Plugin

Run commands:

```sh
$ export VERSION=v1.0.0
$ export PLUGIN_DIR=~/.config/kustomize/plugin/mvochoa.gitlab.com/$VERSION/secretgenerationsecretmanager
$ mkdir -p $PLUGIN_DIR
$ curl -L -o $PLUGIN_DIR/SecretGenerationSecretManager https://gitlab.com/mvochoa/awssecretmanager/-/releases/$VERSION/downloads/binaries/aws_secret_manager_linux_amd64
$ chmod +x $PLUGIN_DIR/SecretGenerationSecretManager
```

## Example file

```yaml
# customGenerator.yaml
apiVersion: mvochoa.gitlab.com/v1.0.0
kind: SecretGenerationSecretManager
metadata:
  name: my-secret
behavior: merge
spec:
  regionAWS: us-west-2
  secrets:
    - name: NAME_SECRET_MANAGER
      rename: NAME_SECRET_KUBERNETES
      isPlainText: true
    - name: NAME_SECRET_MANAGER
      values:
        - name: NAME_SECRET_KUBERNETES
          key: KEY_SECRET_MANAGER
```

```yaml
# kustomization.yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization

generators:
  - customGenerator.yaml
```

Run command `AWS_PROFILE=[profile] kustomize build --enable-alpha-plugins .` result:

```yaml
apiVersion: v1
type: Opaque
metadata:
  name: mySecret-dd8554k82c
data:
  password: SGVsbG8gV29ybGQ=
kind: Secret
```
